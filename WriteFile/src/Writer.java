
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

public class Writer {

	public void writeToFile(String fileName) {
		//get current project directory
		File file = new File(System.getProperty("user.dir") + "/" + fileName);
		
		
		Scanner scan = new Scanner(System.in);
		String text = scan.nextLine();

		BufferedWriter writer = null;
		try {
			file.createNewFile();
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(text);
			writer.newLine();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
